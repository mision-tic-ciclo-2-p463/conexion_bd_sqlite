import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class App {
    public static void main(String[] args) throws Exception {
        
        //Crear conexion a la BD
        Connection conn = DriverManager.getConnection("jdbc:sqlite:hr.db");
        //Objeto para ejecutar sentencias sql
        Statement statement = conn.createStatement();

        //Consultas
        ResultSet employees = statement.executeQuery("SELECT * FROM employees ORDER BY salary DESC");
        while(employees.next()){
            int id = employees.getInt("employee_id");
            String firstname = employees.getString("first_name");
            //Mostrar datos
            System.out.println(id+" - "+firstname);
        }
        //Insertar datos con variables
        String nombre = "Juan";
        String apellido = "Romero";
        PreparedStatement ps = conn.prepareStatement("inser into person(nombre, apellido) values(?, ?)");
        ps.setString(1, nombre);
        ps.setString(2, apellido);
        ps.executeUpdate();

        conn.close();
    }
}
